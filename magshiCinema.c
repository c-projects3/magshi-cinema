/*********************************
* Class: MAGSHIMIM C1			 *
* Week 12           			 *
* Class solution 2  			 *
**********************************/

#include <stdio.h>

#define ROWS 5
#define SEATS_IN_ROW 3

#define CHEAP_TICKET_PRICE 25
#define NORMAL_TICKET_PRICE 35
#define VIP_TICKET_PRICE 50

void printCinema(int cinema[][SEATS_IN_ROW], int rows);
void print_start_msg();
int buy_a_ticket(int cinema[][SEATS_IN_ROW], int row, int col);
int how_many_seat_taken(int cinema[][SEATS_IN_ROW], int len);
void update_price(int cinema[][SEATS_IN_ROW], int len);

int main(void)
{
	int get = 0, flag = 1, row = 0, col = 0, ans =0;
	int cinema[ROWS][SEATS_IN_ROW] = {{CHEAP_TICKET_PRICE, CHEAP_TICKET_PRICE, CHEAP_TICKET_PRICE},
									  {NORMAL_TICKET_PRICE, NORMAL_TICKET_PRICE, NORMAL_TICKET_PRICE},
									  {NORMAL_TICKET_PRICE, VIP_TICKET_PRICE, NORMAL_TICKET_PRICE},
									  {NORMAL_TICKET_PRICE, NORMAL_TICKET_PRICE, NORMAL_TICKET_PRICE},
									  {CHEAP_TICKET_PRICE, CHEAP_TICKET_PRICE, CHEAP_TICKET_PRICE}};
	
	while(flag) 
	{
		print_start_msg();
		scanf("%d", &get);
		switch(get)
		{
			case 1:
				printCinema(cinema, ROWS);
				break;
			case 2:
				printf("Which row (0-4)? ");
				scanf("%d", &row);

				printf("Which seat (0-2)? ");
				scanf("%d", &col);
				ans = buy_a_ticket(cinema, row, col);
				if(ans){
					cinema[row][col] = -1;
					printf("You got the seat!\n");
				}
				else{
					printf("Seat taken!\n");
				}
				break;
			case 3:
				printf("%d taken seats in the cinema\n", how_many_seat_taken(cinema, ROWS));
				break;
			case 4:
				printf("Which row (0-4)? ");
				scanf("%d", &row);

				printf("Which seat (0-2)? ");
				scanf("%d", &col);
				if(buy_a_ticket(cinema, row, col)){
					cinema[row][col] *= 0.9;
				}
				
				break;
			case 5:
				update_price(cinema, ROWS);
				break;
			case 6:
				flag = 0;
				printf("Thank you come again\n");
				break;
			default:
				printf("Invalid input, try again.\n");
				break;
		}
	}
	return 0;
}

//The func print the msg, no input or output
void print_start_msg()
{
	printf("\nWelcome to MagshiCinema\n");
	printf("Select an option:\n");
	printf("1 - Print cinema hall\n");
	printf("2 - Buy a ticket\n");
	printf("3 - Print number of taken seats\n");
	printf("4 - Make a seat discount\n");
	printf("5 - Set a new price for all seats\n");
	printf("6 - Exit\n");
}
/**
Print a cinema hall ticket prices
Input: 	hall, and number of rows
Output: None
*/
void printCinema(int mat[][SEATS_IN_ROW], int rows)
{
	int i = 0, j = 0;

	printf("");
	for(i = 0; i < rows; i++){
		for(j = 0; j < SEATS_IN_ROW; j++){
			printf("%d ", mat[i][j]);
		}
		printf("\n");
	}
}

/*  The func get place to buy, and if it free he save it
	input: cinema - the matrix of the seats in the sinema
	output: 1 - if we replace the place, 0 - if the place is taken*/
int buy_a_ticket(int cinema[][SEATS_IN_ROW], int row, int col){

	if(cinema[row][col] == -1){
		return 0;
	}
	return 1;
}


/*  The func get mat and check how meny places saved (equal to -1)
	input: mat, len of the mat
	output: count - how many places taken*/
int how_many_seat_taken(int cinema[][SEATS_IN_ROW], int len){
	int i = 0, j = 0, count = 0;

	for(i = 0; i < len; i++)
	{
		for(j = 0; j < SEATS_IN_ROW; j++)
		{
			if(cinema[i][j] == -1){ // check if the place saved
				count++;
			}
		}
	}
	return count;
}

/*  The func ask the user for a new price, and update the free place to the new price
	input: mat, len
	output: none*/
void update_price(int cinema[][SEATS_IN_ROW], int len)
{
	int i = 0, j = 0, price = 0;
	printf("Enter new price: ");
	scanf("%d", &price);

	for(i = 0; i < len; i++)
	{
		for(j = 0; j < SEATS_IN_ROW; j++)
		{
			if(cinema[i][j] != -1){ // check if the place not taken
				cinema[i][j] = price;
			}
		}
	}
}
